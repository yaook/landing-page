scss_files = $(filter-out src/scss/_%.scss,$(wildcard src/scss/*.scss))
scss_includes = $(filter src/scss/_%.scss,$(wildcard src/scss/*.scss))
generated_css_files = $(patsubst src/scss/%.scss,static/css/%.css,$(scss_files))

PYTHON3 ?= python3
SCSSC ?= $(PYTHON3) -m scss --load-path src/scss/

build_css: $(generated_css_files)

$(generated_css_files): static/css/%.css: src/scss/%.scss $(scss_files) $(scss_includes)
	mkdir -p static/css/
	$(SCSSC) -o "$@" "$<"

clean:
	rm -f $(generated_css_files)

.PHONY: build_css clean
