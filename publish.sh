#!/bin/bash
set -xeuo pipefail
pip install -r requirements.txt
apt-get update
apt-get install --no-install-recommends npm pandoc -y
npm install -g static-i18n
make
mkdir public
static-i18n --fileFormat yaml --outputOther index.__lng__.html -i en -i de -o public www
cp -r static public/
pushd security-advisories >/dev/null
for name in *.md; do
    pandoc -f markdown -t html "$name" -o "$(basename "$name" .md)".html
done
popd >/dev/null
cp -r security-advisories public/
